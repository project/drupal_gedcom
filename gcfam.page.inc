<?php

/**
 * @file
 * Contains gcfam.page.inc.
 *
 * Page callback for Family records.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Family record templates.
 *
 * Default template: gcfam.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_gcfam(array &$variables) {
  // Fetch GCFam Entity Object.
  $gcfam = $variables['elements']['#gcfam'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
