<?php
/**
 * Created by PhpStorm.
 * User: tilman
 * Date: 05.07.17
 * Time: 00:35
 */

namespace Drupal\Tests\gedcom\Unit;

use Drupal\gedcom\GCDateParser;
use Drupal\Tests\UnitTestCase;

/**
 * Class GCDateParserTest
 * @coversDefaultClass \Drupal\gedcom\GCDateParser
 *
 * @package Drupal\Tests\gedcom\Unit
 * @group gedcomunit
 * @group gedcom
 */
class GCDateParserTest extends \PHPUnit_Framework_TestCase {

  /**
   * @var GCDateParser
   */
  public $dp;

  protected function setUp() {
    $this->dp = new GCDateParser();
  }

  public function test_getDateString() {
    $this->assertEquals('1971-08-16', $this->dp->getDateString('16 AUG 1971'));
    $this->assertEquals('1971-08-01', $this->dp->getDateString('AUG 1971'));
    $this->assertEquals('1971-01-01', $this->dp->getDateString('1971'));
    $this->assertFalse($this->dp->getDateString('BEF APR 1650'));
    $this->assertFalse($this->dp->getDateString('AFT 4 JUL 1650'));
    $this->assertFalse($this->dp->getDateString('BET 1650 AND 1660'));
    $this->assertFalse($this->dp->getDateString('ABT 1650'));
    $this->assertFalse($this->dp->getDateString('EST MAY 1650'));
    $this->assertFalse($this->dp->getDateString('CAL 30 MAY 1650'));
    $this->assertFalse($this->dp->getDateString('FROM 18 APR 2000'));
    $this->assertFalse($this->dp->getDateString('TO 8 MAY 2004'));
  }

  public function test_isValidDate() {
    $this->assertTrue($this->dp->isValidDate('12 APR 1998'));
    $this->assertTrue($this->dp->isValidDate('APR 1998'));
    $this->assertTrue($this->dp->isValidDate('1998'));
    $this->assertFalse($this->dp->isValidDate('BEF APR 1650'));
    $this->assertFalse($this->dp->isValidDate('AFT 4 JUL 1650'));
    $this->assertFalse($this->dp->isValidDate('BET 1650 AND 1660'));
    $this->assertFalse($this->dp->isValidDate('ABT 1650'));
    $this->assertFalse($this->dp->isValidDate('EST MAY 1650'));
    $this->assertFalse($this->dp->isValidDate('CAL 30 MAY 1650'));
    $this->assertFalse($this->dp->isValidDate('FROM 18 APR 2000'));
    $this->assertFalse($this->dp->isValidDate('TO 8 MAY 2004'));
  }

  public function test_isValidDateString() {
    $this->assertTrue($this->dp->isValidDateString('BEF APR 1650'));
    $this->assertTrue($this->dp->isValidDateString('AFT 4 JUL 1650'));
    $this->assertTrue($this->dp->isValidDateString('BET 1650 AND 1660'));
    $this->assertTrue($this->dp->isValidDateString('ABT 1650'));
    $this->assertTrue($this->dp->isValidDateString('EST MAY 1650'));
    $this->assertTrue($this->dp->isValidDateString('CAL 30 MAY 1650'));
    $this->assertTrue($this->dp->isValidDateString('FROM 18 APR 2000'));
    $this->assertTrue($this->dp->isValidDateString('TO 8 MAY 2004'));
    $this->assertTrue($this->dp->isValidDateString('12 APR 1998'));
    $this->assertTrue($this->dp->isValidDateString('APR 1998'));
    $this->assertTrue($this->dp->isValidDateString('1998'));
  }

  public
  function test_isNotValidDateString() {
    $this->assertTrue($this->dp->isValidDateString('12 APR 1998')); //  make sure unimplemented will fail
    $this->assertFalse($this->dp->isValidDateString('12 APRIL 1998'));
  }

  public function test_isValidWithWrongDateOrder() {
    $this->assertFalse($this->dp->isValidDateString('FROM 18 APR 2000 TO 8 MAY 2004'));
    $this->assertFalse($this->dp->isValidDateString('BET 1660 AND 1650'));
  }

  public function test_isExact() {
    $this->assertTrue($this->dp->isExactDate('12 APR 1998'));
    $this->assertFalse($this->dp->isExactDate('APR 1998'));
  }

}
