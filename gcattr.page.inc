<?php

/**
 * @file
 * Contains gcattr.page.inc.
 *
 * Page callback for gcattr entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for gcattr templates.
 *
 * Default template: gcattr.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_gcattr(array &$variables) {
  // Fetch EventEntity Entity Object.
  $gcattr = $variables['elements']['#gcattr'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
