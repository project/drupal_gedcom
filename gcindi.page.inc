<?php

/**
 * @file
 * Contains gcindi.page.inc.
 *
 * Page callback for Individual records.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Individual record templates.
 *
 * Default template: gcindi.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_gcindi(array &$variables) {
  // Fetch GCIndi Entity Object.
  $gcindi = $variables['elements']['#gcindi'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
