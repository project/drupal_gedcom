# ToDo's


## Implement before releasing 1.0
+ Check how php-gecom handles inline notes (probably doesn't)

## Data Model

As described in the [GEDCOM-File structure], there are several items, 
that have not been implemented so far. The following list gives an overview 
of what's missing:

#### Missing strutures
The following structures have not been considered at all so far:
+ <<[HEADER]>>
+ <<[SUBMISSION_RECORD]>>
+ <<[MULTIMEDIA_RECORD]>>
+ <<[REPOSITORY_RECORD]>>
+ <<[SOURCE_RECORD]>>
+ <<[SUBMITTER_RECORD]>>  

#### Incomplete structures
Of the remaining structures, not all fields have been implemented. 
The following is an overview of the missing items.

##### Missing in [FAM_RECORD]
+ FAM.HSUB.AGE
+ FAM.WIFE.AGE
+ FAM.NCHI
+ FAM.SUBM
+ <<[LDS_SPOUSE_SEALING]>>
+ <<[SOURCE_CITATION]>>
+ <<[MULTIMEDIA_LINK]>>
+ REFN <[USER_REFERENCE_NUMBER]>
+ RIN <[AUTOMATED_RECORD_ID]>
+ <<[CHANGE_DATE]>> (change notes)

##### Missing in [INDIVIDUAL_RECORD]
+ INDI.RESN <<[RESTRICTION_NOTICE]>>
+ <<[PERSONAL_NAME_STRUCTURE]>>
    +  INDI.NAME <<[SOURCE_CITATION]>>
+ <<[LDS_INDIVIDUAL_ORDONANCE]>>
+ INDI.SUBM
+ <<[ASSOCIATION_STRUCTURE]>>
+ INDI.ALIA
+ INDI.ANCI
+ INDI.DESI
+ <<[SOURCE_CITATION]>>
+ <<[MULTIMEDIA_LINK]>>
+ INDI.RFN <<[PERMANENT_RECORD_FILE_NUMBER]>>
+ INDI.AFN <<[ANCESTRAL_FILE_NUMBER]>>
+ REFN <[USER_REFERENCE_NUMBER]>
+ RIN <[AUTOMATED_RECORD_ID]>
+ <<[CHANGE_DATE]>> (change notes)


##### Missing in [INDIVIDUAL_EVENT_STRUCTURE]
##### Missing in [INDIVIDUAL_ATTRIBUTE_STRUCTURE]
##### Missing in [EVENT_DETAIL]


##### Missing in [NOTE_RECORD]
In general, notes are supported. However, notes are implemented as 
Drupal 'text' field with 1:n cardinality (to support multiple notes).
However, notes lack 

##### Handling of Notes
Right now, notes 
+ <<[NOTE_STRUCTURE]>>






## Visual design



[GEDCOM-File structure]:http://homepages.rootsweb.ancestry.com/~pmcbride/gedcom/55gcch2.htm#S1
[HEADER]:http://homepages.rootsweb.ancestry.com/~pmcbride/gedcom/55gcch2.htm#HEADER
[FAM_RECORD]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#FAM_RECORD
[INDIVIDUAL_RECORD]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#INDIVIDUAL_RECORD
[NOTE_RECORD]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#NOTE_RECORD
[SUBMISSION_RECORD]:http://homepages.rootsweb.ancestry.com/~pmcbride/gedcom/55gcch2.htm#SUBMISSION_RECORD
[MULTIMEDIA_RECORD]:http://homepages.rootsweb.ancestry.com/~pmcbride/gedcom/55gcch2.htm#MULTIMEDIA_RECORD
[REPOSITORY_RECORD]:http://homepages.rootsweb.ancestry.com/~pmcbride/gedcom/55gcch2.htm#REPOSITORY_RECORD
[SOURCE_RECORD]:http://homepages.rootsweb.ancestry.com/~pmcbride/gedcom/55gcch2.htm#SOURCE_RECORD
[SUBMITTER_RECORD]:http://homepages.rootsweb.ancestry.com/~pmcbride/gedcom/55gcch2.htm#SUBMITTER_RECORD

[PERSONAL_NAME_STRUCTURE]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#PERSONAL_NAME_STRUCTURE
[ASSOCIATION_STRUCTURE]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#ASSOCIATION_STRUCTURE
[INDIVIDUAL_EVENT_STRUCTURE]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#INDIVIDUAL_EVENT_STRUCTURE
[INDIVIDUAL_ATTRIBUTE_STRUCTURE]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#INDIVIDUAL_ATTRIBUTE_STRUCTURE

[EVENT_DETAIL]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#EVENT_DETAIL

[NOTE_STRUCTURE]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#NOTE_STRUCTURE
[LDS_SPOUSE_SEALING]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#LDS_SPOUSE_SEALING
[LDS_INDIVIDUAL_ORDONANCE]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#LDS_INDIVIDUAL_ORDONANCE
[SOURCE_CITATION]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#SOURCE_CITATION
[MULTIMEDIA_LINK]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#MULTIMEDIA_LINK
[CHANGE_DATE]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#CHANGE_DATE
[RESTRICTION_NOTICE]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#RESTRICTION_NOTICE
[PERMANENT_RECORD_FILE_NUMBER]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#PERMANENT_RECORD_FILE_NUMBER
[ANCESTRAL_FILE_NUMBER]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#ANCESTRAL_FILE_NUMBER
[AUTOMATED_RECORD_ID]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#AUTOMATED_RECORD_ID
[USER_REFERENCE_NUMBER]:http://homepages.rootsweb.ancestry.com/%7Epmcbride/gedcom/55gcch2.htm#USER_REFERENCE_NUMBER