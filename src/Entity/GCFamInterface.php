<?php

namespace Drupal\gedcom\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Family records.
 *
 * @ingroup gedcom
 */
interface GCFamInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Family record creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Family record.
   */
  public function getCreatedTime();

  /**
   * Sets the Family record creation timestamp.
   *
   * @param int $timestamp
   *   The Family record creation timestamp.
   *
   * @return \Drupal\gedcom\Entity\GCFamInterface
   *   The called Family record entity.
   */
  public function setCreatedTime($timestamp);

}
