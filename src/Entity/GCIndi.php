<?php

namespace Drupal\gedcom\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\gedcom\GCConfig;
use Drupal\user\UserInterface;
use PhpGedcom\Gedcom;
use PhpGedcom\Record\Indi;

/**
 * Defines the Individual record entity.
 *
 * @ingroup gedcom
 *
 * @ContentEntityType(
 *   id = "gcindi",
 *   label = @Translation("GEDCOM INDI"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gedcom\GCIndiListBuilder",
 *     "views_data" = "Drupal\gedcom\Entity\GCIndiViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\gedcom\Form\GCIndiForm",
 *       "add" = "Drupal\gedcom\Form\GCIndiForm",
 *       "edit" = "Drupal\gedcom\Form\GCIndiForm",
 *       "delete" = "Drupal\gedcom\Form\GCIndiDeleteForm",
 *     },
 *     "access" = "Drupal\gedcom\GedcomAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gedcom\GCIndiHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gcindi",
 *   admin_permission = "administer gedcom",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *   },
 *   links = {
 *     "canonical" = "/gedcom/indi/{gcindi}",
 *     "add-form" = "/gedcom/add/indi",
 *     "edit-form" = "/gedcom/indi/{gcindi}/edit",
 *     "delete-form" = "/gedcom/indi/{gcindi}/delete",
 *     "collection" = "/admin/content/gedcom/indi",
 *   },
 *   field_ui_base_route = "gcindi.settings"
 * )
 */
class GCIndi extends GC implements GCIndiInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  public function updateName() {
    $givn = $this->gc_givn->value;
    $surn = $this->gc_surn->value;
    $birt = $this->gc_birt->entity;
    $deat = $this->gc_deat->entity;
    $text = $givn . ' ' . $surn;
    $text .= ' (*';
    if (isset($birt) || isset($deat)) {
      if (isset($birt) && isset($birt->gc_date_text->value)) {
        $parts = explode('-', $birt->gc_date_text->value);
        $text .= $parts[0];
      }
      else {
        $text .= "??";
      }
      if (isset($deat)) {
        $text .= ' - †';
        if (isset($deat->gc_date_text->value)) {
          $parts = explode('-', $deat->gc_date_text->value);
          $text .= $parts[0];
        }
        else {
          $text .= "??";
        }
      }
    }
    $text .= ')';
    $this->name = $text;
  }

  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $this->updateName();
  }

  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    foreach ($this->gc_fams as $fams) {
      if (($fams_entity = $fams->entity) != null) {
        $fams_entity->updateName();
        $fams_entity->save();
      }
    }
    parent::postSave($storage, $update);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(FALSE)
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the record.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Computed name with birth-and-death dates.'))
      ->setSettings([
        'max_length' => 100,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Create'))
      ->setDescription(t('The time that the record was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the record was last edited.'));

    #
    #   GEDCOM INDI Fields
    #
    $weight = -40;

    $fields['id']
      ->setLabel(t('INDI ID'))
      ->setSettings([
        'prefix' => 'I-',
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => $weight++,
        'settings' => [
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $childOfWeight = $weight++;

    GC::createStringField($fields,
      'npfx', 30,
      t('Name prefix', [], ['context' => 'gedcom']),
      t('Non indexing name piece that appears preceding the given name and surname parts.'), $weight);
    GC::createStringField($fields,
      'givn', 120,
      t('Given name', [], ['context' => 'gedcom']),
      t('Given name or earned name.'), $weight);
    GC::createStringField($fields,
      'nick', 30,
      t('Nickname', [], ['context' => 'gedcom']),
      t('A descriptive or familiar name used in connection with one\'s proper name.'), $weight);
    GC::createStringField($fields,
      'spfx', 30,
      t('Surname prefix', [], ['context' => 'gedcom']),
      t('Surname prefix or article used in a family name. Different surname articles are separated by a comma, for example in the name "de la Cruz", this value would be "de, la".'), $weight);
    GC::createStringField($fields,
      'surn', 120,
      t('Surname', [], ['context' => 'gedcom']),
      t('Surname or family name.'), $weight);
    GC::createStringField($fields,
      'nsfx', 30,
      t('Name suffix', [], ['context' => 'gedcom']),
      t('Non-indexing name piece that appears after the given name and surname parts.'), $weight);

    GC::createOptionField($fields,
      'sex',
      t('Gender', [], ['context' => 'gedcom']),
      t('Gender of the indiviual', [], ['context' => 'gedcom']),
      [
        'M' => t('Male', [], ['context' => 'gedcom']),
        'F' => t('Female', [], ['context' => 'gedcom']),
        'U' => t('Undetermined from available record', [], ['context' => 'gedcom']),
      ], $weight);

    $fact_bundle = [];
    foreach (GCConfig::getINDIFactConfig() as $key => $data) {
      $fact_bundle["gc$key"] = "gc$key";
    }
    $fact_bundle['gcfact'] = "gcfact";
    GC::createBundleReferenceField($fields,
      'fact', 'gcattr', 'gcfact',
      t('Facts', [], ['context' => 'gedcom']),
      t('fact', [], ['context' => 'gedcom']),
      t('Facts (i.e. occupation, religion)'), $weight++, -1)
      ->setSetting('handler_settings', [
        'target_bundles' => $fact_bundle,
      ]);


    GC::createBundleReferenceField($fields,
      'birt', 'gcattr', 'gcbirt',
      t('Birth', [], ['context' => 'gedcom']),
      t('event', [], ['context' => 'gedcom']),
      t('Information about the birth of the individual'), $weight++);
    GC::createBundleReferenceField($fields,
      'deat', 'gcattr', 'gcdeat',
      t('Death', [], ['context' => 'gedcom']),
      t('event', [], ['context' => 'gedcom']),
      t('Information about the death of the individual'), $weight++);

    $event_bundle = [];
    foreach (GCConfig::getINDIEventConfig() as $key => $data) {
      $event_bundle["gc$key"] = "gc$key";
    }
    $event_bundle['gceven'] = "gceven";
    unset($event_bundle['birt']);
    unset($event_bundle['deat']);
    GC::createBundleReferenceField($fields,
      'even', 'gcattr', 'gceven',
      t('More events', [], ['context' => 'gedcom']),
      t('event', [], ['context' => 'gedcom']),
      t('Events (i.e. baptism or burrial)'), $weight++, -1)
      ->setSetting('handler_settings', [
        'target_bundles' => $event_bundle,
      ]);
    GC::createReferenceField($fields,
      'famc', 'gcfam',
      t('Child of', [], ['context' => 'gedcom']),
      t('Child of FAM relation (reference)'), $weight)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'parents',
          'link' => TRUE,
        ],
        'weight' => $childOfWeight,
      ]);
    GC::createOptionField($fields,
      'pedi',
      t('Pedigree', [], ['context' => 'gedcom']),
      t('Gender of the indiviual', [], ['context' => 'gedcom']),
      [
        'adopted' => t('adopted parents', [], ['context' => 'gedcom']),
        'birth' => t('birth parents', [], ['context' => 'gedcom']),
        'foster' => t('included in foster or guardian family', [], ['context' => 'gedcom']),
        'sealing' => t('sealed to parents other than birth', [], ['context' => 'gedcom']),
      ], $weight);
    GC::createReferenceField($fields,
      'fams', 'gcfam',
      t('Parent of', [], ['context' => 'gedcom']),
      t('A parent in FAM relation (reference)'), $weight, -1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'children',
          'link' => TRUE,
        ]
      ]);
    GC::createNoteField($fields,
      'note',
      t('Note', [], ['context' => 'gedcom']),
      t('Notes attached to INDI records'), $weight);

    return $fields;
  }

  /**
   * @param \PhpGedcom\Record\Indi $rec
   */
  public function setFromIndi(Indi $rec, Gedcom $gedcom) {
    $name = current($rec->getName());
    $full_name = $name->getName();
    if ($full_name) {
      $name_break = explode('/', $full_name);
    }
    $this->gc_npfx = $name->getNpfx();
    $this->gc_givn = $name->getGivn() ? $name->getGivn() : $name_break[0];
    $this->gc_nick = $name->getNick();
    $this->gc_spfx = $name->getSpfx();
    $this->gc_surn = $name->getSurn() ? $name->getSurn() : $name_break[1];
    $this->gc_sex = $rec->getSex();

    $this->clearGCAttr(['fact', 'even']);
    $fact_config = GCConfig::getINDIFactConfig();
    foreach ($rec->getAllAttr() as $uc_key => $data) {
      $lc_key = strtolower($uc_key);
      if (isset($fact_config[$lc_key])) {
        $this->addFromFact($lc_key, $data, $gedcom);
      }
      else {
        $this->addFromFact('fact', $data, $gedcom);
      }
    }

    $event_config = GCConfig::getINDIEventConfig();
    foreach ($rec->getAllEven() as $uc_key => $data) {
      $lc_key = strtolower($uc_key);
      switch ($lc_key) {
        case 'birt':
        case 'deat':
          $this->setFromEvent($lc_key, $data, $gedcom);
          break;

        default:
          if (isset($event_config[$lc_key])) {
            $this->addFromEvent($lc_key, $data, $gedcom);
          }
          else {
            $this->addFromEvent('even', $data, $gedcom);
          }
      }
    }

    $this->gc_fams = NULL;
    foreach ($rec->getFams() as $fams) {
      $this->gc_fams[] = self::id2int($fams->getFams());
    }
    $this->gc_famc = NULL;
    $this->gc_pedi = NULL;
    if (($ref = current($rec->getFamc()))) {
      $this->gc_famc = self::id2int($ref->getFamc());
      $this->gc_pedi = $ref->getPedi();
    }
    $this->setNotes($rec, $gedcom);
    $this->setChanged($rec, $this);
  }


  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }
}
