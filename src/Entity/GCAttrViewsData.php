<?php

namespace Drupal\gedcom\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for gcattr entities.
 */
class GCAttrViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    $field_list = ['gc_date_from', 'gc_date_to'];
    $arguments = [
      // Argument type => help text.
      'year' => t('Date in the form of YYYY.'),
      'month' => t('Date in the form of MM (01 - 12).'),
      'day' => t('Date in the form of DD (01 - 31).'),
      'week' => t('Date in the form of WW (01 - 53).'),
      'year_month' => t('Date in the form of YYYYMM.'),
      'full_date' => t('Date in the form of CCYYMMDD.'),
    ];
    foreach ($field_list as $field_name) {
      $data['gcattr'][$field_name]['filter']['id'] = 'datetime';
      $data['gcattr'][$field_name]['filter']['field_name'] = $field_name;

      foreach ($arguments as $argument_type => $help_text) {
        $data['gcattr'][$field_name . '_' . $argument_type] = [
          'title' => $field_name . ' (' . $argument_type . ')',
          'help' => $help_text,
          'argument' => [
            'field' => $field_name,
            'table' => 'gcattr',
            'id' => 'datetime_' . $argument_type,
            'field_name' => $field_name,
            'entity_type' => 'gcattr',
            'empty field name' => t('no value'),
          ],
          'group' => t("GEDCOM Attr"),
        ];
      }
    }
    return $data;
  }
}
