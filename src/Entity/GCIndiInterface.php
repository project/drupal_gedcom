<?php

namespace Drupal\gedcom\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Individual records.
 *
 * @ingroup gedcom
 */
interface GCIndiInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Individual record creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Individual record.
   */
  public function getCreatedTime();

  /**
   * Sets the Individual record creation timestamp.
   *
   * @param int $timestamp
   *   The Individual record creation timestamp.
   *
   * @return \Drupal\gedcom\Entity\GCIndiInterface
   *   The called Individual record entity.
   */
  public function setCreatedTime($timestamp);
}
