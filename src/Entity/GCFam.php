<?php

namespace Drupal\gedcom\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\gedcom\GCConfig;
use Drupal\user\UserInterface;
use PhpGedcom\Gedcom;
use PhpGedcom\Record\Fam;

/**
 * Defines the Family record entity.
 *
 * @ingroup gedcom
 *
 * @ContentEntityType(
 *   id = "gcfam",
 *   label = @Translation("GEDCOM FAM"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gedcom\GCFamListBuilder",
 *     "views_data" = "Drupal\gedcom\Entity\GCFamViewsData",
 *     "form" = {
 *       "default" = "Drupal\gedcom\Form\GCFamForm",
 *       "add" = "Drupal\gedcom\Form\GCFamForm",
 *       "edit" = "Drupal\gedcom\Form\GCFamForm",
 *       "delete" = "Drupal\gedcom\Form\GCFamDeleteForm",
 *     },
 *     "access" = "Drupal\gedcom\GedcomAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gedcom\GCFamHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gcfam",
 *   admin_permission = "administer gedcom",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *   },
 *   links = {
 *     "canonical" = "/gedcom/fam/{gcfam}",
 *     "add-form" = "/gedcom/add/fam",
 *     "edit-form" = "/gedcom/fam/{gcfam}/edit",
 *     "delete-form" = "/gedcom/fam/{gcfam}/delete",
 *     "collection" = "/admin/content/gedcom/fam",
 *   },
 *   field_ui_base_route = "gcfam.settings"
 * )
 */
class GCFam extends GC implements GCFamInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  public function updateName() {
    $husb = $this->gc_husb->entity;
    $wife = $this->gc_wife->entity;
    $text =
      ($husb ? $husb->gc_surn->value : '??')
      . ' - '
      . ($wife ? $wife->gc_surn->value : '??');
    $this->name = $text;
  }

  public function preSave(EntityStorageInterface $storage) {
    $this->updateName();
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(FALSE)
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the record.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Family'))
      ->setDescription(t('Computed family name.'))
      ->setSettings([
        'max_length' => 100,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the record was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the record was last edited.'));

    #
    #   GEDCOM FAM Fields
    #
    $weight = -40;

    $fields['id']
      ->setLabel(t('FAM ID'))
      ->setSettings([
        'prefix' => 'F-',
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => $weight++,
        'settings' => [
          'prefix_suffix' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    GC::createReferenceField($fields,
      'husb', 'gcindi',
      t('Husband', [], ['context' => 'gedcom']),
      t('Husband or father in this relation (Reference)'), $weight);
    GC::createReferenceField($fields,
      'wife', 'gcindi',
      t('Wife', [], ['context' => 'gedcom']),
      t('Wife or mother in this relation (Reference)'), $weight);
    GC::createReferenceField($fields,
      'chil', 'gcindi',
      t('Children', [], ['context' => 'gedcom']),
      t('Children of this relation (References)'), $weight, -1);

    $event_bundle = [];
    foreach (GCConfig::getFAMEventConfig() as $key => $data) {
      $event_bundle["gc$key"] = "gc$key";
    }
    $event_bundle['gceven'] = "gceven";
    GC::createBundleReferenceField($fields,
      'even', 'gcattr', 'gceven',
      t('Family events', [], ['context' => 'gedcom']),
      t("event", [], ['context' => 'gedcom']),
      t('Events (i.e. marriage) in this relation'), $weight, -1)
      ->setSetting('handler_settings', [
        'target_bundles' => $event_bundle,
      ]);
    GC::createNoteField($fields,
      'note',
      t('Note', [], ['context' => 'gedcom']),
      t('Notes attached to FAM records'), $weight);

    return $fields;
  }

  /**
   * @param \PhpGedcom\Record\Fam $rec
   */
  public function setFromFam(Fam $rec, Gedcom $gedcom) {
    if (($id = $rec->getHusb())) {
      $this->gc_husb->target_id = self::id2int($id);
    }
    if (($id = $rec->getWife())) {
      $this->gc_wife->target_id = self::id2int($id);
    }

    $this->clearGCAttr(['even']);
    $event_config = GCConfig::getFAMEventConfig();
    foreach ($rec->getAllEven() as $uc_key => $data) {
      $lc_key = strtolower($uc_key);
      if (isset($event_config[$lc_key])) {
        $this->addFromEvent($lc_key, $data, $gedcom);
      }
      else {
        $this->addFromEvent('even', $data, $gedcom);
      }
    }

    $this->gc_chil = NULL;
    foreach ($rec->getChil() as $chil) {
      $this->gc_chil[] = ['target_id' => self::id2int($chil)];
    }
    $this->setNotes($rec, $gedcom);
    $this->setChanged($rec, $this);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }
}
