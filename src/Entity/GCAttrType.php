<?php

namespace Drupal\gedcom\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the gcattr type entity.
 *
 * @ConfigEntityType(
 *   id = "gcattr_type",
 *   label = @Translation("GEDCOM Attribute type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gedcom\GCAttrTypeListBuilder",
 *     "form" = {
 *       "edit" = "Drupal\gedcom\Form\GCAttrTypeForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\gedcom\GCAttrTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "gcattr_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "gcattr",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/gcattr/{gcattr_type}",
 *     "edit-form" = "/admin/structure/gcattr/{gcattr_type}/edit",
 *     "collection" = "/admin/structure/gcattr"
 *   }
 * )
 */
class GCAttrType extends ConfigEntityBundleBase implements GCAttrTypeInterface {
  /**
   * The gcattr type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The gcattr type label.
   *
   * @var string
   */
  protected $label;

  /**
   * @return string
   */
  public function getLabel(): string {
    return $this->label;
  }

}
