<?php

namespace Drupal\gedcom\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining gcattr entities.
 *
 * @ingroup gcattr
 */
interface GCAttrInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the gcattr creation timestamp.
   *
   * @return int
   *   Creation timestamp of the gcattr.
   */
  public function getCreatedTime();

  /**
   * Sets the gcattr creation timestamp.
   *
   * @param int $timestamp
   *   The gcattr creation timestamp.
   *
   * @return \Drupal\gedcom\Entity\GCAttrInterface
   *   The called gcattr entity.
   */
  public function setCreatedTime($timestamp);
}
