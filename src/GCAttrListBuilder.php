<?php

namespace Drupal\gedcom;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of gcattr entities.
 *
 * @ingroup gcattr
 */
class GCAttrListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID', [], ['context' => 'gedcom']);
    $header['type'] = $this->t('Type', [], ['context' => 'gedcom']);
    $header['name'] = $this->t('Name', [], ['context' => 'gedcom']);
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\gedcom\Entity\EventEntity */
    $row['id'] = $entity->id();
    $row['type'] = $entity->bundle();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.gcattr.edit_form',
      ['gcattr' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
