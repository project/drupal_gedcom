<?php
/**
 * Created by PhpStorm.
 * User: tilman
 * Date: 02.07.17
 * Time: 23:04
 */

namespace Drupal\gedcom;


use Drupal\gedcom\Entity\GCAttr;

class GCConfig {

  private static $generic_event_config;
  private static $generic_fact_config;
  private static $common_event_config;
  private static $indi_fact_config;
  private static $indi_event_config;
  private static $fam_event_config;
  private static $all_attr_config;

  const EVENT_DEFAULT_SHOW = [
    'show_title' => FALSE,
    'show_date' => TRUE,
    'show_todate' => FALSE,
    'show_plac' => TRUE,
  ];

  const FACT_DEFAULT_SHOW = [
    'show_title' => TRUE,
    'show_date' => TRUE,
    'show_todate' => TRUE,
    'show_plac' => TRUE,
  ];

  public static function getGenericEventConfig() {
    if (!isset(GCConfig::$generic_event_config)) {
      GCConfig::$generic_event_config = [
        'even' => [
          'label' => t('Generic event', [], ['context' => 'gedcom']),
          'item_lc' => t('event', [], ['context' => 'gedcom']),
          'item_uc' => t('Event', [], ['context' => 'gedcom']),
          'show_title' => TRUE,
        ],
      ];
    }
    return GCConfig::$generic_event_config;
  }

  public static function getGenericFactConfig() {
    if (!isset(GCConfig::$generic_fact_config)) {
      GCConfig::$generic_fact_config = [
        'fact' => [
          'label' => t('Generic fact', [], ['context' => 'gedcom']),
          'item_lc' => t('fact', [], ['context' => 'gedcom']),
          'item_uc' => t('Fact', [], ['context' => 'gedcom']),
        ],
      ];
    }
    return GCConfig::$generic_fact_config;
  }

  public static function getCommonEventConfig() {
    if (!isset(GCConfig::$common_event_config)) {
      GCConfig::$common_event_config = [
        'cens' => [
          'label' => t('Census event', [], ['context' => 'gedcom']),
          'item_lc' => t('census', [], ['context' => 'gedcom']),
          'item_uc' => t('Census', [], ['context' => 'gedcom']),
          'show_todate' => FALSE,
        ],
      ];
    }
    return GCConfig::$common_event_config;
  }

  public static function getINDIEventConfig() {
    if (!isset(GCConfig::$indi_event_config)) {
      GCConfig::$indi_event_config = [
          //  INDIVIDUAL_EVENT_STRUCTURE: =
          //  [
          //    n[ BIRT | CHR ] [Y|<NULL>]  {1:1}
          //    +1 <<EVENT_DETAIL>>  {0:1}
          //    +1 FAMC @<XREF:FAM>@  {0:1}
          'birt' => [
            'label' => t('Birth event', [], ['context' => 'gedcom']),
            'item_lc' => t('birth', [], ['context' => 'gedcom']),
            'item_uc' => t('Birth', [], ['context' => 'gedcom']),
            'title_label' => t('Birth', [], ['context' => 'gedcom']),
            'date_from_label' => t('Birthdate', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of birth', [], ['context' => 'gedcom']),
            'date_format' => 'gedcom_birthdate',
          ],
          'chr' => [
            'label' => t('Christening event', [], ['context' => 'gedcom']),
            'item_lc' => t('christening', [], ['context' => 'gedcom']),
            'item_uc' => t('Christening', [], ['context' => 'gedcom']),
            'title_label' => t('Christening', [], ['context' => 'gedcom']),
            'date_from_label' => t('Birthdate', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of christening', [], ['context' => 'gedcom']),
          ],
          //    n  [ DEAT | BURI | CREM ] [Y|<NULL>]   {1:1}
          //    +1 <<EVENT_DETAIL>>  {0:1}
          'deat' => [
            'label' => t('Death event', [], ['context' => 'gedcom']),
            'item_lc' => t('death', [], ['context' => 'gedcom']),
            'item_uc' => t('Death', [], ['context' => 'gedcom']),
            'title_label' => t('Death', [], ['context' => 'gedcom']),
            'date_from_label' => t('Deathdate', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of death', [], ['context' => 'gedcom']),
            'date_format' => 'gedcom_deathdate',
          ],
          'buri' => [
            'label' => t('Burrial event', [], ['context' => 'gedcom']),
            'item_lc' => t('burrial', [], ['context' => 'gedcom']),
            'item_uc' => t('Burrial', [], ['context' => 'gedcom']),
            'title_label' => t('Burrial', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of burrial', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of burrial', [], ['context' => 'gedcom']),
          ],
          'crem' => [
            'label' => t('Cremation event', [], ['context' => 'gedcom']),
            'item_lc' => t('cremation', [], ['context' => 'gedcom']),
            'item_uc' => t('Cremation', [], ['context' => 'gedcom']),
            'title_label' => t('Cremation', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of cremation', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of cremation', [], ['context' => 'gedcom']),
          ],
          //    n  ADOP [Y|<NULL>]  {1:1}
          //    +1 <<EVENT_DETAIL>>  {0:1}
          //    +1 FAMC @<XREF:FAM>@  {0:1}
          //      +2 ADOP <ADOPTED_BY_WHICH_PARENT>  {0:1}
          'adop' => [
            'label' => t('Adoption event', [], ['context' => 'gedcom']),
            'item_lc' => t('adoption', [], ['context' => 'gedcom']),
            'item_uc' => t('Adoption', [], ['context' => 'gedcom']),
            'title_label' => t('Adoption', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of adoption', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of adoption', [], ['context' => 'gedcom']),
          ],
          //    n  [ BAPM | BARM | BASM | BLES ] [Y|<NULL>]  {1:1}
          //    +1 <<EVENT_DETAIL>>  {0:1}
          'bapm' => [
            'label' => t('Baptism event', [], ['context' => 'gedcom']),
            'item_lc' => t('baptism', [], ['context' => 'gedcom']),
            'item_uc' => t('Baptism', [], ['context' => 'gedcom']),
            'title_label' => t('Baptism', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of baptism', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of baptism', [], ['context' => 'gedcom']),
          ],
          'barm' => [
            'label' => t('Bar-Mitzvah event', [], ['context' => 'gedcom']),
            'item_lc' => t('bar-mitzvah', [], ['context' => 'gedcom']),
            'item_uc' => t('Bar-Mitzvah', [], ['context' => 'gedcom']),
            'title_label' => t('Bar-Mitzvah', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of bar-mitzvah', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of bar-mitzvah', [], ['context' => 'gedcom']),
          ],
          'basm' => [
            'label' => t('Bat-Mitzvah event', [], ['context' => 'gedcom']),
            'item_lc' => t('bat-mitzvah', [], ['context' => 'gedcom']),
            'item_uc' => t('Bar-Mitzvah', [], ['context' => 'gedcom']),
            'title_label' => t('Bat-Mitzvah', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of bat-mitzvah', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of bat-mitzvah', [], ['context' => 'gedcom']),
          ],
          'bles' => [
            'label' => t('Blessing event', [], ['context' => 'gedcom']),
            'item_lc' => t('blessing', [], ['context' => 'gedcom']),
            'item_uc' => t('Blessing', [], ['context' => 'gedcom']),
            'title_label' => t('Blessing', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of blessing', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of blessing', [], ['context' => 'gedcom']),
          ],
          //    n  [ CHRA | CONF | FCOM | ORDN ] [Y|<NULL>]  {1:1}
          //    +1 <<EVENT_DETAIL>>  {0:1}
          'chra' => [
            'label' => t('Adult christening event', [], ['context' => 'gedcom']),
            'item_lc' => t('adult christening', [], ['context' => 'gedcom']),
            'item_uc' => t('Adult christening', [], ['context' => 'gedcom']),
            'title_label' => t('Adult christening', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of adult christening ', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of adult christening', [], ['context' => 'gedcom']),
          ],
          'conf' => [
            'label' => t('Confirmation event', [], ['context' => 'gedcom']),
            'item_lc' => t('confirmation', [], ['context' => 'gedcom']),
            'item_uc' => t('Confirmation', [], ['context' => 'gedcom']),
            'title_label' => t('Confirmation', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of confirmation', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of confirmation', [], ['context' => 'gedcom']),
          ],
          'fcom' => [
            'label' => t('First Communion event', [], ['context' => 'gedcom']),
            'item_lc' => t('first communion', [], ['context' => 'gedcom']),
            'item_uc' => t('First Communion', [], ['context' => 'gedcom']),
            'title_label' => t('First Communion', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of first communion', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of first communion', [], ['context' => 'gedcom']),
          ],
          'ordn' => [
            'label' => t('Ordination event', [], ['context' => 'gedcom']),
            'item_lc' => t('ordination', [], ['context' => 'gedcom']),
            'item_uc' => t('Ordination', [], ['context' => 'gedcom']),
            'title_label' => t('Ordination', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of ordination', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of ordination', [], ['context' => 'gedcom']),
          ],
          //    n  [ NATU | EMIG | IMMI ] [Y|<NULL>]  {1:1}
          //    +1 <<EVENT_DETAIL>>  {0:1}
          'natu' => [
            'label' => t('Naturalization event', [], ['context' => 'gedcom']),
            'item_lc' => t('naturalization', [], ['context' => 'gedcom']),
            'item_uc' => t('Naturalization', [], ['context' => 'gedcom']),
            'title_label' => t('Naturalization', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of naturalization', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of naturalization', [], ['context' => 'gedcom']),
          ],
          'emig' => [
            'label' => t('Emigration event', [], ['context' => 'gedcom']),
            'item_lc' => t('emigration', [], ['context' => 'gedcom']),
            'item_uc' => t('Emigration', [], ['context' => 'gedcom']),
            'title_label' => t('Emigration', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of emigration', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of emigration', [], ['context' => 'gedcom']),
          ],
          'immi' => [
            'label' => t('Immigration event', [], ['context' => 'gedcom']),
            'item_lc' => t('immigration', [], ['context' => 'gedcom']),
            'item_uc' => t('Immigration', [], ['context' => 'gedcom']),
            'title_label' => t('Immigration', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of immigration', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of immigration', [], ['context' => 'gedcom']),
          ],
          //    n  [ CENS | PROB | WILL] [Y|<NULL>]  {1:1}
          //    +1 <<EVENT_DETAIL>>  {0:1}
          //'cens', ...)  done in GCAttr::createCommonEventRelation
          'prob' => [
            'label' => t('Probate event', [], ['context' => 'gedcom']),
            'item_lc' => t('probate', [], ['context' => 'gedcom']),
            'item_uc' => t('Probate', [], ['context' => 'gedcom']),
            'title_label' => t('Probate', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of probate', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of probate', [], ['context' => 'gedcom']),
          ],
          'will' => [
            'label' => t('Will event', [], ['context' => 'gedcom']),
            'item_lc' => t('will', [], ['context' => 'gedcom']),
            'item_uc' => t('Will', [], ['context' => 'gedcom']),
            'title_label' => t('Will', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of will', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of will', [], ['context' => 'gedcom']),
          ],
          //    n  [ GRAD | RETI ] [Y|<NULL>]  {1:1}
          //    +1 <<EVENT_DETAIL>>  {0:1}
          'grad' => [
            'label' => t('Graduation event', [], ['context' => 'gedcom']),
            'item_lc' => t('graduation', [], ['context' => 'gedcom']),
            'item_uc' => t('Graduation', [], ['context' => 'gedcom']),
            'title_label' => t('Graduation', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of graduation', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of graduation', [], ['context' => 'gedcom']),
          ],
          'reti' => [
            'label' => t('Retirement event', [], ['context' => 'gedcom']),
            'item_lc' => t('retirement', [], ['context' => 'gedcom']),
            'item_uc' => t('Blessing', [], ['context' => 'gedcom']),
            'title_label' => t('Retirement', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of retirement', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of retirement', [], ['context' => 'gedcom']),
          ],
        ] + self::getCommonEventConfig();
    }
    return GCConfig::$indi_event_config;
  }

  public static function getFAMEventConfig() {
    if (!isset(GCConfig::$fam_event_config)) {
      GCConfig::$fam_event_config = [
          //  FAMILY_EVENT_STRUCTURE: =
          //    n [ ANUL | CENS | DIV | DIVF ] [Y|<NULL>]  {1:1}
          //      +1 <<EVENT_DETAIL>>  {0:1}
          'anul' => [
            'label' => t('Annulment event', [], ['context' => 'gedcom']),
            'item_lc' => t('annulment', [], ['context' => 'gedcom']),
            'item_uc' => t('Annulment', [], ['context' => 'gedcom']),
            'title_label' => t('Annulment', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of annulment', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of annulment', [], ['context' => 'gedcom']),
          ],
          //'cens', ...)  done in GCAttr::createCommonEventRelation
          'div' => [
            'label' => t('Divorce event', [], ['context' => 'gedcom']),
            'item_lc' => t('divorce', [], ['context' => 'gedcom']),
            'item_uc' => t('Divorce', [], ['context' => 'gedcom']),
            'title_label' => t('Divorce', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of divorce', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of divorce', [], ['context' => 'gedcom']),
          ],
          'divf' => [
            'label' => t('Divorce filed event', [], ['context' => 'gedcom']),
            'item_lc' => t('divorce filed', [], ['context' => 'gedcom']),
            'item_uc' => t('Divorce filed', [], ['context' => 'gedcom']),
            'title_label' => t('Divorce filed', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of divorce filed', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of divorce filed', [], ['context' => 'gedcom']),
          ],
          //    n [ ENGA | MARR | MARB | MARC ] [Y|<NULL>]  {1:1}
          //      +1 <<EVENT_DETAIL>>  {0:1}
          'enga' => [
            'label' => t('Engagement event', [], ['context' => 'gedcom']),
            'item_lc' => t('engagement', [], ['context' => 'gedcom']),
            'item_uc' => t('Engagement', [], ['context' => 'gedcom']),
            'title_label' => t('Engagement', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of engagement', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of engagement', [], ['context' => 'gedcom']),
          ],
          'marr' => [
            'label' => t('Marriage event', [], ['context' => 'gedcom']),
            'item_lc' => t('marriage', [], ['context' => 'gedcom']),
            'item_uc' => t('Marriage', [], ['context' => 'gedcom']),
            'title_label' => t('Marriage', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of marriage', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of marriage', [], ['context' => 'gedcom']),
          ],
          'marb' => [
            'label' => t('Marriage banns event', [], ['context' => 'gedcom']),
            'item_lc' => t('marriage banns', [], ['context' => 'gedcom']),
            'item_uc' => t('Marriage banns', [], ['context' => 'gedcom']),
            'title_label' => t('Marriage banns', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of marriage banns', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of marriage banns', [], ['context' => 'gedcom']),
          ],
          'marc' => [
            'label' => t('Marriage contract event', [], ['context' => 'gedcom']),
            'item_lc' => t('marriage contract', [], ['context' => 'gedcom']),
            'item_uc' => t('Marriage contract', [], ['context' => 'gedcom']),
            'title_label' => t('Marriage contract', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of marriage contract', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of marriage contract', [], ['context' => 'gedcom']),
          ],
          //    n [ MARL | MARS ] [Y|<NULL>]  {1:1}
          //      +1 <<EVENT_DETAIL>>  {0:1}
          'marl' => [
            'label' => t('Marriage licence event', [], ['context' => 'gedcom']),
            'item_lc' => t('Marriage licence', [], ['context' => 'gedcom']),
            'item_uc' => t('marriage licence', [], ['context' => 'gedcom']),
            'title_label' => t('Marriage licence', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of marriage licence', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of marriage licence', [], ['context' => 'gedcom']),
          ],
          'mars' => [
            'label' => t('Marriage settlement event', [], ['context' => 'gedcom']),
            'item_lc' => t('marriage settlement', [], ['context' => 'gedcom']),
            'item_uc' => t('Marriage settlement', [], ['context' => 'gedcom']),
            'title_label' => t('Marriage settlement', [], ['context' => 'gedcom']),
            'date_from_label' => t('Date of marriage settlement', [], ['context' => 'gedcom']),
            'show_todate' => FALSE,
            'plac_label' => t('Place of marriage settlement', [], ['context' => 'gedcom']),
          ],
          //    n  EVEN          {1:1}
          //      +1 <<EVENT_DETAIL>>  {0:1}
          //'even', ...)  done in GCAttr::createCommonEventRelation
        ] + self::getCommonEventConfig();
    }
    return GCConfig::$fam_event_config;
  }

  public static function getINDIFactConfig() {
    if (!isset(GCConfig::$indi_fact_config)) {
      GCConfig::$indi_fact_config = [
        //  INDIVIDUAL_ATTRIBUTE_STRUCTURE: =
        //    n  CAST <CASTE_NAME>   {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'cast' => [
          'label' => t('Caste fact', [], ['context' => 'gedcom']),
          'item_lc' => t('caste', [], ['context' => 'gedcom']),
          'item_uc' => t('Caste', [], ['context' => 'gedcom']),
          'size' => 90,
          'title_label' => t('Caste', [], ['context' => 'gedcom']),
          'show_date' => FALSE,
          'show_todate' => FALSE,
          'show_plac' => FALSE,
        ],
        //  n  DSCR <PHYSICAL_DESCRIPTION>   {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'dscr' => [
          'label' => t('Description fact', [], ['context' => 'gedcom']),
          'item_lc' => t('description', [], ['context' => 'gedcom']),
          'item_uc' => t('Description', [], ['context' => 'gedcom']),
          'size' => 248,
          'title_label' => t('Description', [], ['context' => 'gedcom']),
          'show_date' => FALSE,
          'show_todate' => FALSE,
          'show_plac' => FALSE,
        ],
        //  n  EDUC <SCHOLASTIC_ACHIEVEMENT>   {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'educ' => [
          'label' => t('Education fact', [], ['context' => 'gedcom']),
          'item_lc' => t('education', [], ['context' => 'gedcom']),
          'item_uc' => t('Education', [], ['context' => 'gedcom']),
          'size' => 248,
          'title_label' => t('Education', [], ['context' => 'gedcom']),
          'date_from_label' => t('Education since', [], ['context' => 'gedcom']),
          'date_to_label' => t('Education until', [], ['context' => 'gedcom']),
          'plac_label' => t('Place of educaction', [], ['context' => 'gedcom']),
        ],
        //  n  IDNO <NATIONAL_ID_NUMBER>   {1:1}*
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'idno' => [
          'label' => t('ID Number fact', [], ['context' => 'gedcom']),
          'item_lc' => t('ID number', [], ['context' => 'gedcom']),
          'item_uc' => t('ID Number', [], ['context' => 'gedcom']),
          'size' => 30,
          'title_label' => t('ID Number', [], ['context' => 'gedcom']),
          'show_date' => FALSE,
          'show_todate' => FALSE,
          'show_plac' => FALSE,
        ],
        //  n  NATI <NATIONAL_OR_TRIBAL_ORIGIN>   {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'nati' => [
          'label' => t('Nationality fact', [], ['context' => 'gedcom']),
          'item_lc' => t('nationality', [], ['context' => 'gedcom']),
          'item_uc' => t('Nationality', [], ['context' => 'gedcom']),
          'size' => 120,
          'title_label' => t('Nationality', [], ['context' => 'gedcom']),
          'date_from_label' => t('Date of nationality', [], ['context' => 'gedcom']),  //TODO: Date gaining nationality?
          'show_todate' => FALSE,
          'plac_label' => t('Place of nationality', [], ['context' => 'gedcom']),  //TODO: Place where nationality was gained?
        ],
        //  n  NCHI <COUNT_OF_CHILDREN>   {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'nchi' => [
          'label' => t('# of Children fact', [], ['context' => 'gedcom']),
          'item_lc' => t('# of children', [], ['context' => 'gedcom']),
          'item_uc' => t('# of Children', [], ['context' => 'gedcom']),
          'size' => 3,
          'title_label' => t('# of Children', [], ['context' => 'gedcom']),
          'show_date' => FALSE,
          'show_todate' => FALSE,
          'show_plac' => FALSE,
        ],
        //  n  NMR <COUNT_OF_MARRIAGES>   {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'nmr' => [
          'label' => t('# of Marriages fact', [], ['context' => 'gedcom']),
          'item_lc' => t('# of marriages', [], ['context' => 'gedcom']),
          'item_uc' => t('# of Marriages', [], ['context' => 'gedcom']),
          'size' => 3,
          'title_label' => t('# of Marriages', [], ['context' => 'gedcom']),
          'show_date' => FALSE,
          'show_todate' => FALSE,
          'show_plac' => FALSE,
        ],
        //  n  OCCU <OCCUPATION>   {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'occu' => [
          'label' => t('Occupation fact', [], ['context' => 'gedcom']),
          'item_lc' => t('occupation', [], ['context' => 'gedcom']),
          'item_uc' => t('Occupation', [], ['context' => 'gedcom']),
          'size' => 90,
          'title_label' => t('Occupation', [], ['context' => 'gedcom']),
          'date_from_label' => t('Occupation from', [], ['context' => 'gedcom']),
          'date_to_label' => t('Occupation until', [], ['context' => 'gedcom']),
          'plac_label' => t('Place of occupation', [], ['context' => 'gedcom']),
        ],
        //  n  PROP <POSSESSIONS>   {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'prop' => [
          'label' => t('Property fact', [], ['context' => 'gedcom']),
          'item_lc' => t('property', [], ['context' => 'gedcom']),
          'item_uc' => t('Property', [], ['context' => 'gedcom']),
          'size' => 248,
          'title_label' => t('Property', [], ['context' => 'gedcom']),
          'date_from_label' => t('Date of property', [], ['context' => 'gedcom']),  //TODO: Date gaining possession?
          'show_todate' => FALSE,
          'plac_label' => t('Place of property', [], ['context' => 'gedcom']),
        ],
        //  n  RELI <RELIGIOUS_AFFILIATION>   {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'reli' => [
          'label' => t('Religion fact', [], ['context' => 'gedcom']),
          'item_lc' => t('religion', [], ['context' => 'gedcom']),
          'item_uc' => t('Religion', [], ['context' => 'gedcom']),
          'size' => 90,
          'title_label' => t('Religion', [], ['context' => 'gedcom']),
          'date_from_label' => t('Date of religion', [], ['context' => 'gedcom']),  //TODO: Date of conversion to religion?
          'show_todate' => FALSE,
          'plac_label' => t('Place of religion', [], ['context' => 'gedcom']),  //TODO: Place of conversion to religion?
        ],
        //  n  RESI           {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'resi' => [
          'label' => t('Residence fact', [], ['context' => 'gedcom']),
          'item_lc' => t('residence', [], ['context' => 'gedcom']),
          'item_uc' => t('Residence', [], ['context' => 'gedcom']),
          'show_title' => FALSE,
          'title_label' => t('Residence', [], ['context' => 'gedcom']),
          'date_from_label' => t('Resident since', [], ['context' => 'gedcom']),
          'date_to_label' => t('Resident until', [], ['context' => 'gedcom']),
          'plac_label' => t('Place of residence', [], ['context' => 'gedcom']),
        ],
        //  n  SSN <SOCIAL_SECURITY_NUMBER>   {0:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'ssn' => [
          'label' => t('Social Security Number fact', [], ['context' => 'gedcom']),
          'item_lc' => t('social security number', [], ['context' => 'gedcom']),
          'item_uc' => t('Social Security Number', [], ['context' => 'gedcom']),
          'size' => 12,
          'title_label' => t('Social Security Number', [], ['context' => 'gedcom']),
          'show_date' => FALSE,
          'show_todate' => FALSE,
          'show_plac' => FALSE,
        ],
        //  n  TITL <NOBILITY_TYPE_TITLE>  {1:1}
        //    +1 <<EVENT_DETAIL>>  {0:1}
        'titl' => [
          'label' => t('Title fact', [], ['context' => 'gedcom']),
          'item_lc' => t('title', [], ['context' => 'gedcom']),
          'item_uc' => t('Title', [], ['context' => 'gedcom']),
          'size' => 120,
          'title_label' => t('Title', [], ['context' => 'gedcom']),
          'date_gc_label' => t('Bestowal: GEDCOM Date', [], ['context' => 'gedcom']),
          'date_from_label' => t('Date of bestowal', [], ['context' => 'gedcom']),
          'show_todate' => FALSE,
          'plac_label' => t('Place of bestowal', [], ['context' => 'gedcom']),
        ],
      ];
    }
    return GCConfig::$indi_fact_config;
  }

  public static function getAllAttrConfig() {
    if (!isset(GCConfig::$all_attr_config)) {
      GCConfig::$all_attr_config = []
        + GCConfig::getGenericEventConfig()
        + GCConfig::getGenericFactConfig()
        + GCConfig::getINDIEventConfig()
        + GCConfig::getINDIFactConfig()
        + GCConfig::getFAMEventConfig();
    }
    return GCConfig::$all_attr_config;
  }
}