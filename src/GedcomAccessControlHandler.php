<?php

namespace Drupal\gedcom;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Family record entity.
 *
 * @see \Drupal\gedcom\Entity\GCFam.
 */
class GedcomAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\gedcom\Entity\GCFamInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view gedcom records');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit gedcom records');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete gedcom records');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add gedcom records');
  }

}
