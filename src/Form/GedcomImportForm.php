<?php


namespace Drupal\gedcom\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gedcom\Entity\GCFam;
use Drupal\gedcom\Entity\GC;
use Drupal\gedcom\Entity\GCIndi;
use PhpGedcom\Gedcom;


class GedcomImportForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gedcom_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['gedcom_file'] = [
      '#type' => 'file',
      '#title' => t('GEDCOM file'),
      '#description' => $this->t('Allowed file extensions: @extensions.', ['@extensions' => 'ged']),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $all_files = $this->getRequest()->files->get('files', []);
    if (!empty($all_files['gedcom_file'])) {
      /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $import_file */
      $import_file = $all_files['gedcom_file'];
      if ($import_file->isValid()) {
        $form_state->setValue('gedcom_file', $import_file->getRealPath());
        $form_state->setValue('gedcom_name', $import_file->getClientOriginalName());
        return;
      }
    }
    $form_state->setErrorByName('gedcom_file', $this->t("The file can't be imported."));
  }


  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file_path = $form_state->getValue('gedcom_file');
    $file_name = $form_state->getValue('gedcom_name');
    $gedcomParser = new \PhpGedcom\Parser();
    $gedcom = $gedcomParser->parse($file_path);
    $test = '';
    GedcomImportForm::importGEDCOM($file_path, $file_name);
  }

  const IMPORT_SLICE_SIZE = 100;

  /**
   * @param string $file_path
   * @param string $file_name
   */
  public static function importGEDCOM(string $file_path, string $file_name) {
    $gedcomParser = new \PhpGedcom\Parser();
    $gedcom = $gedcomParser->parse($file_path);

    $batch = [
      'title' => t("Import of %name", ['%name' => $file_name]),
      'operations' => [
        [[__CLASS__, 'importBatch'], [$gedcom]],
      ],
      'finished' => [__CLASS__, 'importBatchFinished']
    ];
    batch_set($batch);
  }

  public static function importBatch(Gedcom $gedcom, &$context) {
    if (!isset($context['sandbox']['idx'])) {
      $indiCount = count($gedcom->getIndi());
      $famCount = count($gedcom->getFam());
      $context['sandbox']['idx'] = 0;
      $context['sandbox']['max'] = $indiCount + $famCount;
      $context['sandbox']['indi']['new'] = 0;
      $context['sandbox']['indi']['idx'] = 0;
      $context['sandbox']['indi']['max'] = $indiCount;
      $context['sandbox']['fam']['new'] = 0;
      $context['sandbox']['fam']['idx'] = 0;
      $context['sandbox']['fam']['max'] = $famCount;
      //$context['sandbox']['gedcom'] = $gedcom;
      $context['finished'] = 0;
      $context['message'] = t("Import GEDCOM records...");
    }
    else {
      if ($context['sandbox']['indi']['idx'] < $context['sandbox']['indi']['max']) {
        self::importIndiSlice($gedcom, $context);
      }
      else
      if ($context['sandbox']['fam']['idx'] < $context['sandbox']['fam']['max']) {
        self::importFamSlice($gedcom, $context);
      }
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['idx'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * @param $gedcom
   * @param $context
   * @return mixed
   */
  protected static function importIndiSlice(Gedcom $gedcom, &$context) {
    $startIdx = $context['sandbox']['indi']['idx'];
    $slice = array_slice($gedcom->getIndi(), $startIdx, self::IMPORT_SLICE_SIZE);
    foreach ($slice as $current) {
      $id = GC::id2int($current->getId());
      $currentRec = GCIndi::load($id);
      if (isset($currentRec)) {
        //TODO: more sanity checks, if INDI rec with id matches $indiNum
      }
      else {
        $context['sandbox']['indi']['add']++;
        $currentRec = GCIndi::create(["id" => $id]);
      }
      $currentRec->setFromIndi($current, $gedcom);
      $currentRec->save();
      $context['sandbox']['indi']['idx']++;
      $context['sandbox']['idx']++;
    }
    if (isset($currentRec)) {
      $context['message'] = t("Import INDI record %idx of %max: '%name'", [
        '%idx' => $context['sandbox']['indi']['idx'],
        '%max' => $context['sandbox']['indi']['max'],
        '%name' => $currentRec->label(),
      ]);
    }
  }

  /**
   * @param $gedcom
   * @param $context
   * @return mixed
   */
  protected static function importFamSlice(Gedcom $gedcom, &$context) {
    $startIdx = $context['sandbox']['fam']['idx'];
    $slice = array_slice($gedcom->getFam(), $startIdx, self::IMPORT_SLICE_SIZE);
    foreach ($slice as $current) {
      $id = GC::id2int($current->getId());
      $currentRec = GCFam::load($id);
      if (isset($currentRec)) {
        //TODO: more sanity checks, if INDI rec with id matches $indiNum
      }
      else {
        $context['sandbox']['fam']['add']++;
        $currentRec = GCFam::create(["id" => $id]);
      }
      $currentRec->setFromFam($current, $gedcom);
      $currentRec->save();
      $context['sandbox']['fam']['idx']++;
      $context['sandbox']['idx']++;
    }
    if (isset($currentRec)) {
      $context['message'] = t("Import FAM record %idx of %max: '%name'", [
        '%idx' => $context['sandbox']['fam']['idx'],
        '%max' => $context['sandbox']['fam']['max'],
        '%name' => $currentRec->label(),
      ]);
    }
  }

  public static function importBatchFinished($success, $results, $operations) {
    drupal_set_message(t("Successfully imported GEDCOM file"));
  }
}
