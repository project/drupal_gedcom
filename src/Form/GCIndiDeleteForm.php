<?php

namespace Drupal\gedcom\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Individual records.
 *
 * @ingroup gedcom
 */
class GCIndiDeleteForm extends ContentEntityDeleteForm {


}
