<?php

namespace Drupal\gedcom\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting gcattr entities.
 *
 * @ingroup gcattr
 */
class GCAttrDeleteForm extends ContentEntityDeleteForm {


}
