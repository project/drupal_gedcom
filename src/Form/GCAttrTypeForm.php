<?php

namespace Drupal\gedcom\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GCAttrTypeForm.
 *
 * @package Drupal\gedcom\Form
 */
class GCAttrTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $gcattr_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => t('GEDCOM Event type name'),
      '#maxlength' => 255,
      '#default_value' => $gcattr_type->label(),
      '#description' => t("Name of the GEDCOM Event type used on the admin pages."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $gcattr_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\gedcom\Entity\GCAttrType::load',
      ],
      '#disabled' => !$gcattr_type->isNew(),
    ];

    $form['show_title'] = [
      '#type' => 'checkbox',
      '#title' => t('Title field', [], ['context' => 'gedcom']),
      '#default_value' => $gcattr_type->get('show_title'),
      '#description' => t("If enabled, users can enter data in the 'Title' field of this bundle"),
    ];

    $form['title_label'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => t('Text of Title field label', [], ['context' => 'gedcom']),
      '#description' => t("Leave empty if you want to use GEDCOM module's default values."),
      '#default_value' => $gcattr_type->get('title_label'),
    ];

    $form['show_date'] = [
      '#type' => 'checkbox',
      '#title' => t('Date field', [], ['context' => 'gedcom']),
      '#description' => t("If enabled, users can enter data in the 'Date' field of this bundle and see the 'from' part (if date is range or period)"),
      '#default_value' => $gcattr_type->get('show_date'),
    ];

    $form['date_gc_label'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => t('Text of GEDCOM Date input field label', [], ['context' => 'gedcom']),
      '#description' => t("Leave empty if you want to use GEDCOM module's default values."),
      '#default_value' => $gcattr_type->get('date_gc_label'),
    ];

    $form['date_from_label'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => t('Text of Date (from) field label', [], ['context' => 'gedcom']),
      '#description' => t("Leave empty if you want to use GEDCOM module's default values."),
      '#default_value' => $gcattr_type->get('date_from_label'),
    ];

    $form['show_todate'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Date (to)', [], ['context' => 'gedcom']),
      '#description' => t('If enabled, users can see the Date (to) field of this bundle'),
      '#default_value' => $gcattr_type->get('show_todate'),
      '#required' => FALSE,
    ];

    $form['date_to_label'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => t('Text of <i>Date</i> (to) field label', [], ['context' => 'gedcom']),
      '#description' => t("Leave empty if you want to use GEDCOM module's default values."),
      '#default_value' => $gcattr_type->get('date_to_label'),
    ];

    $form['show_plac'] = [
      '#type' => 'checkbox',
      '#title' => t('Place field', [], ['context' => 'gedcom']),
      '#description' => t('If enabled, users can edit the Place field of this bundle'),
      '#default_value' => $gcattr_type->get('show_plac'),
      '#required' => FALSE,
    ];

    $form['plac_label'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => t('Text of Place field label', [], ['context' => 'gedcom']),
      '#description' => t("Leave empty if you want to use GEDCOM module's default values."),
      '#default_value' => $gcattr_type->get('plac_label'),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $gcattr_type = $this->entity;
    $status = $gcattr_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message(t('Created the %label GEDCOM Attribute type.', [
          '%label' => $gcattr_type->label(),
        ]));
        break;

      default:
        drupal_set_message(t('Saved the %label GEDCOM Attribute type.', [
          '%label' => $gcattr_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($gcattr_type->toUrl('collection'));
  }

}
