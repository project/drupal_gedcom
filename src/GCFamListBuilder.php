<?php

namespace Drupal\gedcom;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Family records.
 *
 * @ingroup gedcom
 */
class GCFamListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('FAM ID');
    $header['name'] = $this->t('Name', [], ['context' => 'gedcom']);
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\gedcom\Entity\GCFam */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.gcfam.edit_form', array(
          'gcfam' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
